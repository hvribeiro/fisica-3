# Física 3

*Notas de aula do curso de Física 3 - DFI/UEM*

Vamos usar o livro [**Curso de Física Básica: Eletromagnetismo. Vol. 3. Nussenzveig, Herch Moysés.**](http://library.lol/main/5E5DC6B6ABE22DE9998530A0667656BD)

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/fisica_iii.pdf).


## Parte 1 - Lei de Coulomb, Campo Elétrico, Potencial Elétrico e Capacitância

- [Aula 01 - 09/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula01.png) | [Vídeo](https://drive.google.com/file/d/1xnJPDlxD5jqjZdIrgbr_xxDhuAYhK01J/view?usp=sharing)
- [Aula 02 - 11/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula02.png) | [Vídeo](https://drive.google.com/file/d/14jHEfuqrZ1U7tReIRXGe8G_to4inhER-/view?usp=sharing)
- [Aula 03 - 16/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula03.png) | [Vídeo](https://drive.google.com/file/d/1Haq7p5W6OP-CGJIIdO-l7JAGll-AErvp/view?usp=sharing)
- [Aula 04 - 18/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula04.png) | [Vídeo](https://drive.google.com/file/d/1zH1Z5iLcL2Eq09_26s6tGw1_OLAMeTRA/view?usp=sharing)
- [Aula 05 - 23/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula05.png) | [Vídeo](https://drive.google.com/file/d/1jkJ14Iq8q_kx0hy3ksSbkIdFxc364n7P/view?usp=sharing)
- [Aula 06 - 25/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula06.png) | [Vídeo](https://drive.google.com/file/d/12hT4FHuRo0tekFNlbdvRBEf8GAJpDvdC/view?usp=sharing)
- [Aula 07 - 30/08/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula07.png) | [Vídeo](https://drive.google.com/file/d/1T8ai_vETxrBSHXhvVrUFkAvPdlrWbIs8/view?usp=sharing)
- [Aula 08 - 01/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula08.png) | [Vídeo](https://drive.google.com/file/d/1EGGOKPsRInd7YcY8vGMEomLIYNjM_rOy/view?usp=sharing)
- [Aula 09 - 08/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula09.png) | [Vídeo](https://drive.google.com/file/d/1zn-apeHjV8qoDP0dGWKsrF3uUIbLPsv7/view?usp=sharing)
- [Aula 10 - 13/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula10.png) | [Vídeo](https://drive.google.com/file/d/1aVKqgJRkwObgoWtGwZa_OdQf3gcTqThj/view?usp=sharing)
- [Aula 11 - 15/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula11.png) | [Vídeo](https://drive.google.com/file/d/1bjUnrk0vhqjdqE5AXWtCOA6fdDpwj2-V/view?usp=sharing)
- [Aula 12 - 20/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula12.png) | [Vídeo](https://drive.google.com/file/d/1O5-ik4csTY3u968YQNtngNzJdVgfeyut/view?usp=sharing)
- [Aula 13 - 22/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula13.png) | [Vídeo](https://drive.google.com/file/d/1Wh-fGP__7v9ALDFdC9u9_q8LwC5q7PIm/view?usp=sharing)
- [Aula 14 - 27/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula14.png) | [Vídeo](https://drive.google.com/file/d/1NX7jVfNHgWJtvE66jx2vHcg3-h2ilq1n/view?usp=sharing)
- [Aula 15 - 29/09/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula15.png) | [Vídeo](https://drive.google.com/file/d/1Nne4cDbTpC2rydJtsrOhzQCMjZW7rXUC/view?usp=sharing)

## Parte 2 - Corrente Elétrica, Campo Magnético, Lei de Ampère e Lei de Faraday

- [Aula 16 - 04/10/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula16.png) | [Vídeo](https://drive.google.com/file/d/1XjvM9uNO-D7M3uEZ-mAn63mBtsCgj7xH/view?usp=sharing)
- [Aula 17 - 06/10/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula17.png) | [Vídeo](https://drive.google.com/file/d/1bF3ZpZ5VoCkBKl_CifeEG3oZs8uz97Rh/view?usp=sharing)
- [Aula 18 - 18/10/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula18.png) | [Vídeo](https://drive.google.com/file/d/1xxYMm_7eJijBxhj-nVTHjFLWfzIMv8tf/view?usp=sharing)
- [Aula 19 - 20/10/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula19.png) | [Vídeo](https://drive.google.com/file/d/1_c4as1O_DAjQezB-GlPtqCUEoCS9lHCY/view?usp=sharing)
- [Aula 20 - 25/10/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula20.png) | [Vídeo](https://drive.google.com/file/d/1xwF0V2GVXlEYQAmx2d_afCJNM_ibeZoK/view?usp=sharing)
- [Aula 21 - 27/10/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula21.png) | [Vídeo](https://drive.google.com/file/d/1mUyNBuaOi6mAIOf-Jbl-Nzknp3sgTSIG/view?usp=sharing)
- [Aula 22 - 01/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula22.png) | [Vídeo](https://drive.google.com/file/d/1mqmf7xod7J0rOQ3gw0dY5RlJxwSbcc_w/view?usp=sharing)
- [Aula 23 - 03/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula23.png) | [Vídeo](https://drive.google.com/file/d/1qwcrQu6Apa-_WRBZeyDOHVLXxOoa8uDu/view?usp=sharing)
- [Aula 24 - 08/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula24.png) | [Vídeo](https://drive.google.com/file/d/1gyL9juQV3evCdAEh-EEpTHFrw_Pngsxd/view?usp=sharing)
- [Aula 25 - 10/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula25.png) | [Vídeo](https://drive.google.com/file/d/1bkSGL9O27ZbZmNa-36mEp68Nk5YViqZy/view?usp=sharing)
- [Aula 26 - 17/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula26.png) | [Vídeo](https://drive.google.com/file/d/1pEOWDFmhw_0qNePHE2gQ83IJKnQUy5pq/view?usp=sharing)
- [Aula 27 - 22/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula27.png) | [Vídeo](https://drive.google.com/file/d/1dFpFsdIBj5EIcxylDWunhtb7d5N_c7oQ/view?usp=sharing)
- [Aula 28 - 24/11/2021](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula28.png) | [Vídeo](https://drive.google.com/file/d/1MkfUGwV5Z3eYijgNnA0HvjQAVDetE9LO/view?usp=sharing)
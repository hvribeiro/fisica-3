# Física 3

*Notas de aula do curso de Física 3 - DFI/UEM*

Vamos usar o livro [**Curso de Física Básica: Eletromagnetismo. Vol. 3. Nussenzveig, Herch Moysés.**](http://93.174.95.29/main/5E5DC6B6ABE22DE9998530A0667656BD)

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/fisica_iii.pdf).


## Parte 1 - Lei de Coulomb, Campo Elétrico, Potencial Elétrico e Capacitância

- [Aula 01 - 17/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula01.png) | [Vídeo](https://drive.google.com/file/d/1QkrUKyW0w4IBhqY9wFgVheL0yD6JAAe9/view?usp=sharing)
- [Aula 02 - 19/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula02.png) | [Vídeo](https://drive.google.com/file/d/12BW6khd9VuogE82sxQIPYEgGw8JJz_Q6/view?usp=sharing)
- [Aula 03 - 24/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula03.png) | [Vídeo](https://drive.google.com/file/d/1OUuYNC11LoO46fFD7AP0TlF2_h37_MnG/view?usp=sharing)
- [Aula 04 - 26/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula04.png) | [Vídeo](https://drive.google.com/file/d/1iZYvZ1ROP_up5migRmA7pIcD_6mYBo8O/view?usp=sharing)
- [Aula 05 - 31/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula05.png) | [Vídeo](https://drive.google.com/file/d/11tfpxeYPcT82lx5wS2DOmu4MkE59fbrP/view?usp=sharing)
- [Aula 06 - 02/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula06.png) | [Vídeo](https://drive.google.com/file/d/1mx13ISD7hqucU1iXJfoCJqM5GiKHNy-U/view?usp=sharing)
- [Aula 07 - 09/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula07.png) | [Vídeo](https://drive.google.com/file/d/1vpRJeMfCal-xzaRvlcg16UWCR7km6BYB/view?usp=sharing)
- [Aula 08 - 14/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula08.png) | [Vídeo](https://drive.google.com/file/d/1xVXmQm6IayTyNHLdR9U8W_dyGQviIS1a/view?usp=sharing)
- [Aula 09 - 16/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula09.png) | [Vídeo](https://drive.google.com/file/d/1bu5leWQ41KjOw7Nv1iwVJTfyX51mZ-HT/view?usp=sharing)
- [Aula 10 - 21/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula10.png) | [Vídeo](https://drive.google.com/file/d/1acq55Wdo2VhkVhLQ3SR5iEU2xCiw-dWg/view?usp=sharing)
- [Aula 11 - 23/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula11.png) | [Vídeo](https://drive.google.com/file/d/1RLIsnn3vBTZxz3Pq57ps2bFl7bHCPDvZ/view?usp=sharing)
- [Aula 12 - 28/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula12.png) | [Vídeo](https://drive.google.com/file/d/10VnKcW3pcZHI2RaUlV7A151EDBp8xOC-/view?usp=sharing)
- [Aula 13 - 30/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte1/aula13.png) | [Vídeo](https://drive.google.com/file/d/1uiu6JrhR2VYNFr-Ur5Po8ZQyDWYlZV2M/view?usp=sharing)

## Parte 2 - Corrente Elétrica, Campo Magnético, Lei de Ampère e Lei de Faraday

- [Aula 14 - 05/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula14.png) | [Vídeo](https://drive.google.com/file/d/1QuPHgInJXxsBiokHTmdkdckAN_a1ni3e/view?usp=sharing)
- [Aula 15 - 07/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula15.png) | [Vídeo](https://drive.google.com/file/d/1-uRVgPg8ggLl_vAhyyj8UDF8_66-wgXV/view?usp=sharing)
- [Aula 16 - 14/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula16.png) | [Vídeo](https://drive.google.com/file/d/1UZqQ2me1rut4CUMFY3yCnUXNlKa6WHXn/view?usp=sharing)
- [Aula 17 - 19/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula17.png) | [Vídeo](https://drive.google.com/file/d/1ALu3gjp4A3p0OoTDnaTiB6-P0S5xTS8y/view?usp=sharing)
- [Aula 18 - 21/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula18.png) | [Vídeo](https://drive.google.com/file/d/1uO99ZyZTCcp3My_31IyVEFK-FViYVuXK/view?usp=sharing)
- [Aula 19 - 26/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula19.png) | [Vídeo](https://drive.google.com/file/d/1ayaIOpjSt6t8DI2ewpzrBS5onLMr64rW/view?usp=sharing)
- [Aula 20 - 04/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula20.png) | [Vídeo](https://drive.google.com/file/d/1lwP84iOOHTvSOP2KcM26gEJj6kqcY1oM/view?usp=sharing)
- [Aula 21 - 09/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula21.png) | [Vídeo](https://drive.google.com/file/d/1x7YZc2LW6AT7v_4JKsBVUHpTDvINdZF2/view?usp=sharing)
- [Aula 22 - 11/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula22.png) | [Vídeo](https://drive.google.com/file/d/1CNSE1CbG9RmWbC7Zg2eS9K33Fg_XBp9_/view?usp=sharing)
- [Aula 23 - 16/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula23.png) | [Vídeo](https://drive.google.com/file/d/1ruO7qvkoh1YAOYXaRGrFPEduuD73mZCc/view?usp=sharing)
- [Aula 24 - 18/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula24.png) | [Vídeo](https://drive.google.com/file/d/1LSGMPjD3kFawcCUiKLIDgyUDmlX-1VTK/view?usp=sharing)
- [Aula 25 - 23/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula25.png) | [Vídeo](https://drive.google.com/file/d/1uS22-AiiPAo1OMNaOAQomx-OCVw1oeJ6/view?usp=sharing)
- [Aula 26 - 25/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula26.png) | [Vídeo](https://drive.google.com/file/d/1tFTjZEupM8lhoKo8tk2IIpJ33AhMSy9R/view?usp=sharing)
- [Aula 27 - 30/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula27.png) | [Vídeo](https://drive.google.com/file/d/1e2wfinbfAEkJo4AQYvIkjyOisJzz1N39/view?usp=sharing)
- [Aula 28 - 02/12/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/anos_anteriores/2020/parte2/aula28.png) | [Vídeo](https://drive.google.com/file/d/10Qejhg8S-LcEjHVdlQGIf_0MKRnE3qMa/view?usp=sharing)